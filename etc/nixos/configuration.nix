# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Allow installation of unfree packages.
  nixpkgs.config.allowUnfree = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "quicksilver"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  # i18n = {
  #   consoleFont = "Lat2-Terminus16";
  #   consoleKeyMap = "us";
  #   defaultLocale = "en_US.UTF-8";
  # };

  # Set your time zone.
  time.timeZone = "Asia/Yekaterinburg";

  boot.supportedFilesystems = [ "ntfs" ];
  
  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    wget curl tmux htop mc emacs git networkmanagerapplet
    firefox thunderbird pcmanfm gimp pavucontrol leiningen
    blueman mplayer transmission-gtk gnome3.file-roller
    acpilight zathura guile leafpad clojure openjdk gnumake
    docker-compose slack maven gradle boot httpie python3
    nodejs povray imagemagick pass
    texlive.combined.scheme-medium
    nmap-graphical firejail
    flex bison gcc clang
    xfce.xfce4-taskmanager
    xfce.xfce4-systemload-plugin
    xfce.xfce4-sensors-plugin
    xfce.xfce4-mpc-plugin
    xfce.xfce4-netload-plugin
    xfce.xfce4-xkb-plugin
  ];

  virtualisation.virtualbox.host.enable = true;
  virtualisation.docker.enable = true;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  services.openssh.authorizedKeysFiles = [".ssh/authorized_keys"];
  services.openssh.passwordAuthentication = false;
  services.openssh.permitRootLogin = "no";


  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;
  networking.extraHosts =
  ''
    192.168.0.100	argentum
  '';

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;
  hardware.pulseaudio.package = pkgs.pulseaudioFull; # support for bluetooth headsets
  hardware.bluetooth.enable = true;
  hardware.pulseaudio.support32Bit = true;

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.xserver.layout = "us,ru";
  services.xserver.xkbOptions = "grp:caps_toggle,grp_led:caps";

  # Enable touchpad support.
  services.xserver.libinput.enable = true;
  services.xserver.libinput.tapping = false;

  # Enable XFCE.
  services.xserver.desktopManager = {
      xfce.enable = true;
      default = "xfce";
      xfce.thunarPlugins = [ pkgs.xfce.thunar-archive-plugin ];
  };

  # Enable KDE.
  # services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;

  # Enable Openbox.
  # services.xserver.windowManager.openbox.enable = true;

  # Enable GNOME
  # services.xserver.desktopManager.gnome3.enable = true;
  
  # Enable Pantheon
  # services.xserver.desktopManager.pantheon.enable = true;

  # Compositor
  services.compton = {
      enable = true;
      fade = true;
      inactiveOpacity = "0.9";
      shadow = true;
      shadowOffsets = [ (-5) (-5) ];
      fadeDelta = 4;
      extraOptions = ''
                     shadow-radius = 3;
                     #Window type settings
                     wintypes:
                     {
                       tooltip = { fade = true; shadow = false; };
                       menu = { shadow = false; };
                       dropdown_menu = { shadow = false; };
                       popup_menu =  { shadow = false; };
                     };
                     '';
  };
  
  # Enable OpenGL for 32-bit apps.
  hardware.opengl.driSupport32Bit = true;

  fonts = {
    enableFontDir = true;
    enableGhostscriptFonts = true;
    fonts = with pkgs; [
      source-code-pro
      inconsolata
      fira-mono
      ubuntu_font_family
    ];
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  # users.users.jane = {
  #   isNormalUser = true;
  #   extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
  # };
  users.users.nervous = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" "docker" ]; # Enable ‘sudo’ for the user.
  };

  # Automatically collect garbage
  nix.gc.automatic = true;
  # nix.gc.dates = "*:45";
  nix.gc.options = ''--delete-older-than 30d'';
  
  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.03"; # Did you read the comment?

}
