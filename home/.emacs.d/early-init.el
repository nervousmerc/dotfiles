;; straight.el: prevent package.el from loading packages at startup
(setq package-enable-at-startup nil)
