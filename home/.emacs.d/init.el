;;;; main configuration file


;;;; helper functions

(defun config/windows? ()
  (string-equal system-type "windows-nt"))

(defun config/old-emacs? ()
  (version< emacs-version "27.0"))

(defun load-user-file (file)
  (interactive "f")
  "Load a file in current user's configuration directory."
  (load (expand-file-name file user-init-dir)))


;;;; package management: `straight.el'

;; bootstrap

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el"
                         user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; integrate with `use-package'

(straight-use-package 'use-package)
(setq straight-use-package-by-default t)

;; tie related commands into a family of short bindings with a common prefix
(use-package hydra)

;; bind `straight.el' commands to keys
(defhydra hydra-straight (:hint nil)
  "
_c_heck all   |_f_etch all |_m_erge all       |_n_ormalize all |p_u_sh all
_C_heck pkg   |_F_etch pkg |_M_erge pkg       |_N_ormlize pkg  |p_U_sh pkg
------------^^+----------^^+----------------^^+--------------^^+------------||_q_uit||
_r_ebuild all |_p_ull all  |_v_ersions freeze |_w_atcher start |_g_et recipe
_R_ebuild pkg |_P_ull pkg  |_V_ersions thaw   |_W_atcher quit  |prun_e_ build"
  ("c" straight-check-all)
  ("C" straight-check-package)
  ("r" straight-rebuild-all)
  ("R" straight-rebuild-package)
  ("f" straight-fetch-all)
  ("F" straight-fetch-package)
  ("p" straight-pull-all)
  ("P" straight-pull-package)
  ("m" straight-merge-all)
  ("M" straight-merge-package)
  ("n" straight-normalize-all)
  ("N" straight-normalize-package)
  ("u" straight-push-all)
  ("U" straight-push-package)
  ("v" straight-freeze-versions)
  ("V" straight-thaw-versions)
  ("w" straight-watcher-start)
  ("W" straight-watcher-quit)
  ("g" straight-get-recipe)
  ("e" straight-prune-build)
  ("q" nil))

(global-set-key (kbd "C-c s") 'hydra-straight/body)


;; startup profiles

(when (getenv "FAST") (setq use-package-always-ensure nil))
(when (getenv "STATS") (setq use-package-compute-statistics t))


;; user-defined initialization

;; where to find user init files
(defconst user-init-dir
  (expand-file-name "elisp" user-emacs-directory))

;; load user init files
(load-user-file "common")
(load-user-file "theme")
(load-user-file "org")
(load-user-file "prog")
(load-user-file "markup")
(load-user-file "try")

;; custom configuration location
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
  (load custom-file))
