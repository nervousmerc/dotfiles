;;;; disabled packages --- may be useful someday


;;;; init

;; guix elisp packages path
;; (setq guix-emacs-load-path
;;       (expand-file-name "~/.guix-profile/share/emacs/site-lisp/"))
;; (add-to-list 'load-path guix-emacs-load-path)


;;;; common

;; inobtrusively remove trailing whitespace on save
;; (use-package ws-butler
;;   :diminish
;;   :config
;;   (ws-butler-global-mode t))

;; touch-typing exercises
;; (use-package speed-type)

;; =^___________^=
;; animations seem to leak memory
;; (use-package nyan-mode
;;   :config
;;   (nyan-mode 1)
;;   (setq nyan-bar-length 16)
;;   (setq nyan-wavy-trail nil))

;; a minimal modeline, use
;; M-x mood-line-mode to enable
;; (use-package mood-line)

;; a simple mode-line
;; (use-package simple-modeline
;;   :hook (after-init . simple-modeline-mode))

;; a fancy and fast mode-line inspired by minimalism design
;; (use-package doom-modeline
;;   :ensure t
;;   :hook (after-init . doom-modeline-mode))

;; a minor-mode menu for the mode line
;; (use-package minions)

;; tabs and ribbons for the mode-line
;; (use-package moody
;;   :config
;;   (setq x-underline-at-descent-line t)
;;   (moody-replace-mode-line-buffer-identification)
;;   (moody-replace-vc-mode)
;;   (moody-replace-eldoc-minibuffer-message-function))

;; shorten major mode names by using a set of user-defined rules
;; (use-package cyphejor
;;   :config
;;   (setq cyphejor-rules
;;         '(:upcase
;;           ("clojurescript-mode" "ωλ")
;;           ("buffer"      "β")
;;           ("dired"       "δ")
;;           ("emacs-lisp"  "ε")
;;           ("inferior"    "i" :prefix)
;;           ("interaction" "i" :prefix)
;;           ("interactive" "i" :prefix)
;;           ("ELisp"       "ελ" :postfix)
;;           ("shell"       "sh" :postfix)
;;           ("text"        "ξ"))))

;; display emacs mode line in minibuffer
;; (use-package mini-modeline
;;   :diminish
;;   :config
;;   (mini-modeline-mode t))

;; (use-package magit-todos
;;   :after (magit)
;;   :config (let ((inhibit-message t))
;;             (magit-todos-mode 1))
;;   (transient-append-suffix 'magit-status-jump '(0 0 -1)
;;     '("T " "Todos" magit-todos-jump-to-todos)))

;; visiting previous versions of files
;; (use-package git-timemachine)

;; working with git forges (github, gitlab, gitea etc)
;; (use-package forge
;;   :after magit)

;; (use-package image-dired)

;; (use-package image-dired+
;;   :after image-dired)

;; shows unbound keys
;; (use-package free-keys)

;; fringe settings
;; (use-package fringe
;;   :custom
;;   (fringe-mode '(8 . 0)))

;; Google translate support
;; (use-package google-translate
;;   :config
;;   (setq google-translate-default-source-language "en")
;;   (setq google-translate-default-target-language "ru")
;;   (setq google-translate-pop-up-buffer-set-focus t)
;;   (setq google-translate-show-phonetic nil)
;;   :bind
;;   (("C-c t" . 'google-translate-at-point)
;;    ("C-c T" . 'google-translate-query-translate)
;;    ("C-c r" . 'google-translate-at-point-reverse)
;;    ("C-c R" . 'google-translate-query-translate-reverse)))

;; Guix package manager interface
;; (use-package guix
;;   :bind
;;   (("C-x x" . guix-popup)))

;; desktop notifications
;; (use-package alert
;;   :config
;;   (setq alert-default-style 'notifications))

;; piper
;; (use-package piper
;;   :disabled
;;   :load-path "~/.emacs.d/git/emacs-piper/"
;;   :bind ("C-c C-|" . piper))

;; better help
;; (use-package helpful
;;   :bind
;;   (:map help-mode-map
;;         ("f" . #'helpful-callable)
;;         ("v" . #'helpful-variable)
;;         ("k" . #'helpful-key)
;;         ("F" . #'helpful-at-point)
;;         ("F" . #'helpful-function)
;;         ("C" . #'helpful-command)))

;; github repos as package sources
;; (use-package quelpa)
;; (use-package quelpa-use-package)

;; view logs
;; (use-package logview)

;; modeline
;; (use-package spaceline)
;; (use-package spaceline-config
;;   :ensure spaceline
;;   :config
;;   (spaceline-emacs-theme))

;; disabled: font size adjustment breaks it
;; display fill column indicator
;; (use-package fill-column-indicator
;;   :custom
;;   (fci-rule-width 1)
;;   (fci-rule-color "cadetBlue4")
;;   (fci-rule-column 80)
;;   :hook (prog-mode . fci-mode))

;; modeline icons
;; (use-package mode-icons
;;   :config
;;   (mode-icons-mode -1)
;;   (setq mode-icons-change-mode-name nil)
;;   (setq mode-icons-grayscale-transform t)
;;   (setq mode-icons-desaturate-inactive t)
;;   (setq mode-icons-desaturate-active t))

;; open very large files
;; (use-package vlf)

;; spell checking
;; (use-package flyspell)

;; screensaver
;; (use-package zone
;;   :config
;;   (setq zone-programs [zone-nyan])
;;   (zone-when-idle 3600))
;; (use-package zone-nyan
;;   :after zone)

;; eshell made handy (requires quelpa)
;; (use-package eshell-toggle
;;   :custom
;;   (eshell-toggle-size-fraction 3)
;;   (eshell-toggle-use-projectile-root t)
;;   (eshell-toggle-run-command nil)
;;   ;; (eshell-toggle-init-function #'eshell-toggle-init-ansi-term)
;;   (eshell-toggle-init-function #'eshell-toggle-init-eshell)
;;   :quelpa
;;   (eshell-toggle :repo "4DA/eshell-toggle" :fetcher github :version original)
;;   :bind
;;   ("C-`" . eshell-toggle))

;; (use-package treemacs-icons-dired
;;   :after (dired)
;;   :config (treemacs-icons-dired-mode))

;; (use-package treemacs-evil
;;   :after (treemacs evil))

;; emoji support
;; (use-package emojify
;;   :hook (after-init . global-emojify-mode))

;; (use-package dired-x)

;; treemacs
;; (use-package treemacs
;;   :defer t
;;   :init
;;   (with-eval-after-load 'winum
;;     (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
;;   :config
;;   (progn
;;     (setq treemacs-collapse-dirs                 (if treemacs-python-executable 3 0)
;;           treemacs-deferred-git-apply-delay      0.5
;;           treemacs-display-in-side-window        t
;;           treemacs-eldoc-display                 t
;;           treemacs-file-event-delay              5000
;;           treemacs-file-follow-delay             0.2
;;           treemacs-follow-after-init             t
;;           treemacs-git-command-pipe              ""
;;           treemacs-goto-tag-strategy             'refetch-index
;;           treemacs-indentation                   2
;;           treemacs-indentation-string            " "
;;           treemacs-is-never-other-window         nil
;;           treemacs-max-git-entries               5000
;;           treemacs-missing-project-action        'ask
;;           treemacs-no-png-images                 nil
;;           treemacs-no-delete-other-windows       t
;;           treemacs-project-follow-cleanup        nil
;;           treemacs-persist-file                  (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
;;           treemacs-position                      'left
;;           treemacs-recenter-distance             0.1
;;           treemacs-recenter-after-file-follow    nil
;;           treemacs-recenter-after-tag-follow     nil
;;           treemacs-recenter-after-project-jump   'always
;;           treemacs-recenter-after-project-expand 'on-distance
;;           treemacs-show-cursor                   nil
;;           treemacs-show-hidden-files             t
;;           treemacs-silent-filewatch              nil
;;           treemacs-silent-refresh                nil
;;           treemacs-sorting                       'alphabetic-desc
;;           treemacs-space-between-root-nodes      t
;;           treemacs-tag-follow-cleanup            t
;;           treemacs-tag-follow-delay              1.5
;;           treemacs-width                         24)

;;     ;; The default width and height of the icons is 22 pixels. If you are
;;     ;; using a Hi-DPI display, uncomment this to double the icon size.
;;     ;;(treemacs-resize-icons 44)

;;     (treemacs-follow-mode t)
;;     (treemacs-filewatch-mode t)
;;     (treemacs-fringe-indicator-mode t)
;;     (pcase (cons (not (null (executable-find "git")))
;;                  (not (null treemacs-python-executable)))
;;       (`(t . t)
;;        (treemacs-git-mode 'deferred))
;;       (`(t . _)
;;        (treemacs-git-mode 'simple))))
;;   :bind
;;   (:map global-map
;;         ([f8] . treemacs)
;;         ("M-0"       . treemacs-select-window)
;;         ("C-x t 1"   . treemacs-delete-other-windows)
;;         ("C-x t t"   . treemacs)
;;         ("C-x t B"   . treemacs-bookmark)
;;         ("C-x t C-t" . treemacs-find-file)
;;         ("C-x t M-t" . treemacs-find-tag)))

;; (use-package treemacs-projectile
;;   :after (treemacs projectile))

;; (use-package treemacs-magit
;;   :after (treemacs magit))


;;;; EXWM

;; Emacs X Window Manager
;; (require 'exwm)
;; (require 'exwm-config)
;; (exwm-config-default)


;;;; federation

;; (use-package matrix-client
;;   :disabled
;;   :quelpa ((matrix-client
;;             :fetcher github :repo "alphapapa/matrix-client.el"
;;             :files (:defaults "logo.png" "matrix-client-standalone.el.sh"))))


;;;; guix

;; emacs interface for Guix
;; (require 'guix-autoloads)
;; (global-set-key (kbd "C-x x") 'guix-popup)


;;;; mail

;; ;; mu4e is way too old in Ubuntu 16.04, get it with Guix
;; (require 'mu4e)

;; ;; use mu4e for e-mail in emacs
;; (setq mail-user-agent 'mu4e-user-agent)

;; (setq mu4e-mu-binary "/home/nervous/.guix-profile/bin/mu")
;; (setq mu4e-maildir "/home/nervous/Documents/mail/nervousmerc@gmail.com")
;; (setq mu4e-drafts-folder "/[Gmail].Drafts")
;; (setq mu4e-sent-folder   "/[Gmail].Sent Mail")
;; (setq mu4e-trash-folder  "/[Gmail].Trash")

;; ;; HTML messages rendering
;; ;; (setq mu4e-html2text-command
;; ;;   "html2markdown | grep -v '&nbsp_place_holder;'")
;; (setq mu4e-html2text-command "html2text -utf8 -width 120")

;; ;; don't save message to Sent Messages, Gmail/IMAP takes care of this
;; (setq mu4e-sent-messages-behavior 'delete)

;; ;; (See the documentation for `mu4e-sent-messages-behavior' if you have
;; ;; additional non-Gmail addresses and want assign them different
;; ;; behavior.)

;; ;; setup some handy shortcuts
;; ;; you can quickly switch to your Inbox -- press ``ji''
;; ;; then, when you want archive some messages, move them to
;; ;; the 'All Mail' folder by pressing ``ma''.

;; (setq mu4e-maildir-shortcuts
;;     '( (:maildir "/INBOX"              :key ?i)
;;        (:maildir "/[Gmail].Sent Mail"  :key ?s)
;;        (:maildir "/[Gmail].Trash"      :key ?t)
;;        (:maildir "/[Gmail].All Mail"   :key ?a)))

;; ;; allow for updating mail using 'U' in the main view:
;; (setq mu4e-get-mail-command "offlineimap")

;; ;; something about ourselves
;; (setq
;;    user-mail-address "nervousmerc@gmail.com"
;;    user-full-name  "Пигалев Константин"
;;    mu4e-compose-signature
;;     (concat
;;       "Пигалев Константин"))

;; ;; sending mail -- replace USERNAME with your gmail username
;; ;; also, make sure the gnutls command line utils are installed
;; ;; package 'gnutls-bin' in Debian/Ubuntu

;; (require 'smtpmail)
;; (setq message-send-mail-function 'smtpmail-send-it
;;    starttls-use-gnutls t
;;    smtpmail-starttls-credentials '(("smtp.gmail.com" 587 nil nil))
;;    smtpmail-auth-credentials
;;      '(("smtp.gmail.com" 587 "nervousmerc@gmail.com" nil))
;;    smtpmail-default-smtp-server "smtp.gmail.com"
;;    smtpmail-smtp-server "smtp.gmail.com"
;;    smtpmail-smtp-service 587)

;; ;; alternatively, for emacs-24 you can use:
;; ;;(setq message-send-mail-function 'smtpmail-send-it
;; ;;     smtpmail-stream-type 'starttls
;; ;;     smtpmail-default-smtp-server "smtp.gmail.com"
;; ;;     smtpmail-smtp-server "smtp.gmail.com"
;; ;;     smtpmail-smtp-service 587)

;; ;; don't keep message buffers around
;; (setq message-kill-buffer-on-exit t)


;;;; prog

;; typescript
;; (use-package typescript-mode)

;; (use-package tern
;;   :diminish
;;   :config
;;   (add-hook 'js-mode-hook (lambda () (tern-mode t))))

;; vue.js
;; (use-package vue-mode
;;   :diminish
;;   :config
;;   (setq mmm-submode-decoration-level 0)
;;   ;; :hook (mmm-mode . (lambda ()
;;   ;; (set-face-background 'mmm-default-submode-face "#fafafa")))
;;   )

;; (use-package xref-js2)

;; (use-package js2-refactor
;;   :diminish
;;   :config
;;   (add-hook 'js2-mode-hook #'js2-refactor-mode)
;;   (js2r-add-keybindings-with-prefix "C-c r"))

;; go
;; (use-package go-mode
;;   :hook (before-save . gofmt-before-save)
;;   :bind (:map go-mode-map
;;               (("M-." . godef-jump)
;;                ("C-c i" . go-goto-imports)
;;                ("C-c C-r" . go-remove-unused-imports))))

;; prettier autoformatter
;; (use-package prettier-js
;;   :hook
;;   ((js2-mode web-mode) . prettier-js-mode))

;; rust
;; (use-package rustic)

;; groovy
;; (use-package groovy-mode)

;; Nix confuguration files
;; (use-package nix-mode)

;; Emacs Speaks Statistics
;; (use-package ess)

;; yasnippet
;; (use-package yasnippet
;;   :diminish
;;   :config
;;   (yas-global-mode 1))

;; (use-package yasnippet-snippets)

;; Jupyter notebooks
;; (use-package jupyter)

;; Emacs IPython Notebooks
;; (use-package ein)

;; Hashicorp Terraform
;; (use-package terraform-mode)

;; Common Lisp: SLIME
;; (progn
;;   (load (expand-file-name "~/.quicklisp/slime-helper.el"))
;;   (setq inferior-lisp-program "sbcl"))

;; syntax analysis library
;; exists as external package and built-in in recent emacsen

;; (use-package tree-sitter
;;   :config
;;   (global-tree-sitter-mode 1))
;; (use-package tree-sitter-langs)

;; communicate with inferior clojure process
;; (use-package inf-clojure)

;; enabling eglot to start in subfolders of a monorepo
;; https://michael.stapelberg.ch/posts/2021-04-02-emacs-project-override/
;; https://blog.jmthornton.net/p/emacs-project-override
;; https://gist.github.com/pesterhazy/e8e445e6715f5d8bae3c62bc9db32469?permalink_comment_id=4355363
;; (defun clojure-subproject (dir)
;;   "Return a Clojure subproject dir for the current dir."
;;   (let ((override (locate-dominating-file dir "deps.edn")))
;;     (if override
;;         (cons 'projectile override)
;;       nil)))

;; (use-package project
;;   :config
;;   (add-hook 'project-find-functions
;;             #'clojure-subproject))
;;
;; actually it is sufficient to add .projectile file at the subproject root;
;; deps.edn should have the same effect but no

;; (use-package lsp-mode
;;   :init
;;   (setq lsp-keymap-prefix "C-c l")
;;   :hook ((js-mode . lsp-deferred)
;;          (typescript-mode . lsp-deferred)
;;          (go-mode . lsp-deferred)
;;          (c-mode . lsp-deferred)
;;          ;; (clojure-mode . lsp-deferred)
;;          (lsp-mode . lsp-enable-which-key-integration))
;;   :commands (lsp lsp-deferred))

;; (use-package lsp-ui
;;   :config
;;   (setq lsp-ui-doc-show-with-cursor nil)
;;   (setq lsp-ui-sideline-show-code-actions nil)
;;   :commands lsp-ui-mode)

;; (use-package lsp-ivy
;;   :commands lsp-ivy-workspace-symbol)

;; (use-package lsp-treemacs
;;   :commands lsp-treemacs-errors-list)
;; optionally if you want to use debugger
;; (use-package dap-mode)
;; (use-package dap-LANGUAGE) to load the dap adapter for your language

;; python
;; (use-package elpy)

;; scheme
;; (use-package geiser
;;   :config
;;   (use-package geiser-mit)
;;   (use-package geiser-guile)
;;   (use-package geiser-racket)
;;   (setq geiser-default-implementation 'racket))

;; Lox programming language from Robert Nystrom's book "Crafting Interpreters"
;; (use-package lox-mode)

;; the Superior Lisp Interaction Mode for Emacs
;; (use-package slime
;;   :config
;;   (setq slime-repl-shortcut-dispatch-char ?\?)
;;   (setq inferior-lisp-program "sbcl --noinform")
;;   :bind (:map slime-mode-map
;;               ("C-c b" . slime-eval-buffer)
;;               ("C-c C-q" . slime-repl-quit)))


;;;; markup

;; jq scripts
;; (use-package jq-mode
;;   :after (json-mode)
;;   :bind (:map json-mode-map
;;               (("C-c C-j" . #'jq-interactively))))

;; ABC music notation
;; (use-package abc-mode)

;; (use-package fb2-reader
;;   :mode ("\\.fb2\\(.zip\\|\\)$" . fb2-reader-mode))


;;;; themes

;; (add-to-list 'custom-theme-load-path
;;              (expand-file-name "themes" user-emacs-directory))
;; (load-theme 'gotham t)


;;;; org

;; (use-package ob-http)
;; (use-package ob-restclient)

;; external org was broken after update to 9.6 (missing macro org-assert-version)
;; (use-package org
;;   :diminish
;;   :config
;;   (setq org-directory (expand-file-name "~/Documents/org"))
;;   ;; capture
;;   ;; not getting set when simply put in config, so needs a workaround
;;   ;; https://github.com/syl20bnr/spacemacs/issues/9748
;;   ;; (setq org-default-notes-file (concat org-directory "/notes.org"))
;;   (with-eval-after-load 'org
;;     (setq org-default-notes-file (concat org-directory "/notes.org")))
;;   (setq org-default-log-file (concat org-directory "/log.org"))
;;   (setq org-capture-templates
;;         '(("t" "Todo" entry (file+headline org-default-notes-file "Tasks")
;;            "* TODO %?\n %i\n %U\n %a")
;;           ("i" "Idea" entry (file+headline org-default-notes-file "Ideas")
;;            "* %?\n %i\n %U\n %a\n")
;;           ("j" "Journal" entry (file+olp+datetree org-default-log-file)
;;            "* %? %U\n  %i\n  %a")))
;;   (setq org-refile-targets '((nil :maxlevel . 2)
;;                              ;; all top-level headlines in the
;;                              ;; current buffer are used (first) as a
;;                              ;; refile target
;;                              (org-agenda-files :maxlevel . 1)))
;;   ;; appearance
;;   (setq org-clock-mode-line-total 'current)
;;   (setq org-bullets-bullet-list '("○" "○" "○" "○"))
;;   (setq org-adapt-indentation t)
;;   ;; LaTeX fragments preview
;;   (plist-put org-format-latex-options :scale 2.0)
;;   ;; export
;;   (setq org-export-backends '(ascii beamer html icalendar latex odt md))
;;   (setq org-export-dispatch-use-expert-ui nil)
;;   ;; (setq org-latex-default-packages-alist
;;   ;;       '(("AUTO" "inputenc" t
;;   ;;          ("xetex"))
;;   ;;         ("T2A" "fontenc" t
;;   ;;          ("xetex"))
;;   ;;         ("" "microtype" t)
;;   ;;         ("" "graphicx" t)
;;   ;;         ("" "grffile" t)
;;   ;;         ("" "longtable" nil)
;;   ;;         ("" "wrapfig" nil)
;;   ;;         ("" "rotating" nil)
;;   ;;         ("normalem" "ulem" t)
;;   ;;         ("" "amsmath" t)
;;   ;;         ("" "amsfonts" t)
;;   ;;         ("" "textcomp" t)
;;   ;;         ("" "amssymb" t)
;;   ;;         ("" "capt-of" nil)
;;   ;;         ("no-math" "fontspec" t)
;;   ;;         ;; ("unicode=true, colorlinks=true" "hyperref" nil)
;;   ;;         ))
;;   ;; enable languages for babel source blocks
;;   (org-babel-do-load-languages
;;    'org-babel-load-languages
;;    '((shell . t)
;;      (python . t)
;;      (clojure . t)
;;      (latex . t)
;;      (plantuml . t)
;;      (R . t)
;;      (ditaa . t)
;;      (calc . t)
;;      (java . t)
;;      (sqlite . t)))
;;   ;; set paths to files needed by some languages
;;   (setq org-plantuml-jar-path
;;         (expand-file-name "~/Programs/plantuml/plantuml.jar"))
;;   (setq org-ditaa-jar-path
;;         (expand-file-name "~/Programs/ditaa/ditaa.jar"))
;;   ;; disable source code block evaluation confirmation
;;   (setq org-confirm-babel-evaluate nil)
;;   ;; source code blocks - default headers
;;   (setq org-babel-default-header-args
;;         '((:session . "none")
;;           (:results . "value verbatim replace")
;;           (:exports . "code")
;;           (:cache . "no")
;;           (:noweb . "no")
;;           (:hlines . "no")
;;           (:tangle . "no")))
;;   ;; HTML export viewer
;;   (setq org-file-apps
;;         (quote
;;          ((auto-mode . emacs)
;;           ("\\.mm\\'" . default)
;;           ("\\.x?html?\\'" . "/usr/bin/firefox %s")
;;           ("\\.pdf\\'" . default))))
;;   :bind
;;   (("C-c l". org-store-link)
;;    ("C-c c" . org-capture)
;;    ("C-c a" . org-agenda)
;;    (:map org-mode-map
;;          ("C-x p" . org-pomodoro))))

;; references for org-roam files
;; (use-package org-ref
;;   :after (org-roam)
;;   :config
;;   (setq reftex-default-bibliography
;;         (list (expand-file-name "bibliography/bibliography.bib"
;;                                 org-roam-directory)))
;;   (setq org-ref-bibliography-notes (expand-file-name "bibliography/notes.org"
;;                                                      org-roam-directory)
;;         org-ref-default-bibliography reftex-default-bibliography
;;         org-ref-pdf-directory ""))

;; (use-package org-journal
;;   :config
;;   (setq org-journal-dir "~/Documents/org/journal/"
;;         org-journal-date-format "%A, %d %B %Y"))

;; (use-package org-kanban)

;; (use-package ob-clojurescript)

;; (use-package htmlize)

;; (use-package org-alert)

;; (use-package org-download
;;   :after org
;;   :bind
;;   (:map org-mode-map
;;         (("s-Y" . org-download-screenshot)
;;          ("s-y" . org-download-yank))))

;; (use-package jupyter)

;; a spaced-repetition system for Emacs' org-mode.
;; cannot be so easily installed, so was postponed
;; (use-package org-fc)

;; autocompletion
;; (use-package company
;;   :diminish
;;   :config
;;   (setq company-backends
;;         (remove 'company-ropemacs company-backends)
;;         company-tooltip-limit
;;         20
;;         company-tooltip-align-annotations t)
;;   (global-company-mode 1)
;;   :bind
;;   (("<menu>" . 'company-complete)))

;; a company front-end (popup menu) with icons
;; (use-package company-box
;;   :diminish
;;   :hook (company-mode . company-box-mode))

;; a mode for quickly browsing, filtering, and editing directories of plain text
;; notes
;; (use-package deft
;;   :after org-roam
;;   :config
;;   (setq deft-directory org-roam-directory)
;;   (setq deft-extensions '("org" "md"))
;;   (setq deft-default-extension "org")
;;   (setq deft-recursive t)
;;   (setq deft-use-filename-as-title nil)
;;   (setq deft-use-filter-string-for-filename t)
;;   :bind
;;   ("C-x /" . 'deft))


;;;; themes

;; (use-package gotham-theme)

;; (use-package zenburn-theme)

;; (use-package berrys-theme
;;   :config
;;   (setq-default cursor-type '(bar . 2))
;;   (setq-default line-spacing 2))


;;;; misc

;; automatic highlighting of text patterns
;; (use-package hi-lock
;;   :straight (:type built-in)
;;   :config
;;   (global-hi-lock-mode 1))
