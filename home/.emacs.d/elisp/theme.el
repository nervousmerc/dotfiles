;;;; packages with themes


(use-package gruvbox-theme
  :config
  (load-theme 'gruvbox-dark-hard t))
