;;;; packages related to org-mode

(defface org-error-face
  '((t (:background "red" :foreground "black")))
  "Face for highlighting error/alert signs.")
(defface org-warning-face
  '((t (:background "dark orange" :foreground "black")))
  "Face for highlighting warning/question signs.")
(defface org-success-face
  '((t (:background "lime green" :foreground "black")))
  "Face for highlighting success/good signs.")
(defface org-info-face
  '((t (:background "deep sky blue" :foreground "black")))
  "Face for highlighting info/peculiarity signs.")
(defvar org-error 'org-error-face)
(defvar org-warning 'org-warning-face)
(defvar org-success 'org-success-face)
(defvar org-info 'org-info-face)

;; major mode for keeping notes,authoring documents, computational notebooks,
;; literate programming, maintaining to-do lists, planning projects, and more
(use-package org
  ;; it is a built-in package, do not try to fetch it
  :straight (:type built-in)
  :bind
  (("C-c a" . org-agenda)
   ("C-c c" . org-capture)
   ("C-c l" . org-store-link)
   :map org-mode-map
   ("C-c SPC" . org-table-blank-field)
   ("C-c M-," . org-insert-structure-template))
  :init
  ;; http/REST client for org-mode
  (use-package verb
    :config
    (setq verb-auto-kill-response-buffers t))
  :config
  ;;;; general settings
  ;; convoluted key bindings --- a keymap on a prefix within another keymap
  (define-key org-mode-map (kbd "C-c C-r") verb-command-map)
  ;; root directory for org files
  (setq org-directory (expand-file-name "~/Documents/org"))
  ;; default file for capturing notes
  (setq-default org-default-notes-file
                (concat org-directory "/notes.org"))
  ;; agenda setup
  (setq-default org-agenda-files
                (list (file-truename (concat org-directory
                                             "/roam/tasks"))))
  (setq org-agenda-custom-commands
        '(("f" occur-tree "FIXME")))
  ;; default todo states
  (setq org-todo-keywords
        '((sequence
           "TODO(t)" "INPROGRESS(i)" "PAUSED(p)" "|" "DONE(d)" "CANCELLED(c)")))
  ;; default todo faces
  (setq org-todo-keyword-faces
        '(("INPROGRESS" . "dark orange")
          ("APPLIED" . "dark orange")
          ("INVITED" . "light green")
          ("ACCEPTED" . "light green")
          ("REJECTED" . "light blue")
          ("DROPPED" . "light blue")
          ("CANCELLED" . "light blue")
          ("ARCHIVED" . "gray")
          ("DONE" . "light green")))
  (setq org-enforce-todo-dependencies t)
  (setq org-log-into-drawer t)
  ;; logging changes in deadlines
  (setq org-log-redeadline t)
  ;; defaults for inline images
  (setq org-image-actual-width (list 400))
  ;; LaTeX fragments preview scale
  (plist-put org-format-latex-options :scale 1.8)
  ;; indent section text to align it with the headline name
  (setq org-adapt-indentation t)
  ;;;; source code blocks
  ;; enable languages
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((shell . t)
     (python . t)
     (clojure . t)
     (scheme .t)
     (verb . t)
     (plantuml . t)))
  ;; default headers
  (setq org-babel-default-header-args
        '((:session . "none")
          (:results . "value replace")
          (:exports . "code")
          (:cache . "no")
          (:noweb . "no")
          (:hlines . "no")
          (:tangle . "no")))
  ;; default backend for Clojure source code blocks
  (setq org-babel-clojure-backend 'cider)
  ;; disable evaluation confirmation
  (setq org-confirm-babel-evaluate nil)
  ;; PlantUML jar path
  (setq org-plantuml-jar-path
        (expand-file-name "~/Programs/plantuml/plantuml.jar"))
  ;; highlight some text patterns
  (font-lock-add-keywords 'org-mode
                          '(("(!)" . (0 org-error t))
                            ("(\\?)" . (0 org-warning t))
                            ("(\\$)" . (0 org-success t))
                            ("(@)" . (0 org-info t)))
                          'prepend)
  :hook
  (org-mode . auto-fill-mode))


;; pomodoro timer for org-mode
(use-package org-pomodoro
  :defer t
  :config
  (setq org-pomodoro-format "%s"))


;; custom bullets for outline headers
(use-package org-bullets
  :demand t
  :config
  (setq org-bullets-bullet-list '("○" "○" "○" "○"))
  :hook
  (org-mode . org-bullets-mode))


;; plain-text personal knowledge management system based on org-mode
(use-package org-roam
  :diminish
  :init
  (setq org-roam-v2-ack t)
  (setq-default org-roam-directory (file-truename "~/Documents/org/roam"))
  (setq-default org-roam-node-display-template
                (concat "${title:*} "
                        (propertize "${tags:10}" 'face 'org-tag)))
  (setq-default org-roam-mode-sections
                '((org-roam-backlinks-section :unique t)
                  org-roam-reflinks-section))
  (setq-default org-roam-capture-templates
                '(("d" "default" plain
                   "* Tags\n\n  - %?\n\n* ${title}"
                   :target (file+head "%<%Y%m%d%H%M%S>-${slug}.org"
                                      "#+title: ${title}")
                   :empty-lines 1
                   :unnarrowed t)))
  (add-to-list 'org-roam-capture-templates
               `("t" "task" plain
                 ,(concat
                   "* Tags\n\n  - %?\n\n"
                   "* ${title} (replace with problem statement later)\n"
                   "** Description\n\n"
                   "   Symptoms, observations, reports, requests\n\n"
                   "** Problem\n\n"
                   "   Unmet user objectives and causes of that\n\n"
                   "** Approach\n\n"
                   "   The selected approach to the solution\n\n"
                   "** Log\n\n"
                   "   The log of the design process --- understanding of the "
                   "problem, selecting\n"
                   "   an approach; with use cases, decision matrices, "
                   "diagrams, etc")
                 :target (file+head "tasks/${slug}.org"
                                    "#+title: ${title}\n#+filetags: :task:")
                 :empty-lines 1
                 :unnarrowed t))
  (add-to-list 'org-roam-capture-templates
               '("e" "encrypted" plain "* Tags\n\n  - %?\n\n* ${title}"
                 :target (file+head "%<%Y%m%d%H%M%S>-${slug}.org.gpg"
                                    "#+title: ${title}")
                 :empty-lines 1
                 :unnarrowed t))
  (add-to-list 'org-roam-capture-templates
               '("l" "literature notes" plain "* Tags\n\n  - %?\n\n* ${title}"
                 :target (file+head "%<%Y%m%d%H%M%S>-${slug}.org"
                                    "#+title: ${title}\n#+filetags: :notes:")
                 :empty-lines 1
                 :unnarrowed t))
  (add-to-list 'org-roam-capture-templates
               '("j" "journal entry" entry "* ${title} [%<%H:%M>]\n"
                 :target (file+datetree "journal.org" day)
                 :kill-buffer t
                 :empty-lines 1
                 :unnarrowed t))
  (add-to-list 'org-roam-capture-templates
               '("f" "fleeting note" entry "* ${title}\n\n  %?"
                 :target (file+head "notes.org"
                                    "#+title: fleeting notes")
                 :kill-buffer t
                 :empty-lines 1
                 :unnarrowed t))
  (add-to-list 'org-roam-capture-templates
               '("c" "company" plain
                 (file "~/Documents/workspace/personal/employment/templates/company.org")
                 :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
                 :empty-lines 1
                 :unnarrowed t))
  :config
  (org-roam-setup)
  (require 'org-roam-export)
  (require 'org-roam-protocol)
  (setq org-roam-database-connector 'sqlite-builtin)
  :bind (("C-c n B" . org-roam-buffer-toggle)
         ;; ("C-c n B" . org-roam-buffer-display-dedicated)
         ("C-c n f" . org-roam-node-find)
         ("C-c n g" . org-roam-graph)
         ("C-c n i" . org-roam-node-insert)
         ("C-c n c" . org-roam-capture)
         ("C-c n d" . org-roam-dailies-capture-today)
         ("C-c n t" . org-roam-dailies-goto-today)
         ("C-c n U" . org-roam-db-sync)))

;; convenience functions for org-roam with the help of consult (enhanced
;; completing-read)
(use-package consult-org-roam
  :after org-roam
  :init
  (require 'consult-org-roam)
  ;; override default org-roam completion functions
  (consult-org-roam-mode 1)
  :bind
  ;; Define some convenient keybindings as an addition
  ("C-c n e" . consult-org-roam-file-find)
  ("C-c n b" . consult-org-roam-backlinks)
  ("C-c n l" . consult-org-roam-forward-links)
  ("C-c n r" . consult-org-roam-search))


;; web frontend for org-roam
(use-package org-roam-ui
  :bind (("C-c n u" . org-roam-ui-open)))


;; spaced-repetition (flashcards) system for org-mode
(use-package org-fc
  :straight
  (org-fc :type git
          :host nil
          :repo "https://git.sr.ht/~l3kn/org-fc"
          :files (:defaults "awk" "demo.org"))
  :custom
  (org-fc-directories '("~/Documents/org/flashcards"))
  :config
  (require 'org-fc-hydra)
  :bind
  ("C-c f" . org-fc-hydra/body))


;; plot org tables
(use-package gnuplot
  :defer t)


;; convert buffer text and decorations to HTML
(use-package htmlize
  :defer t)


;;;; manual customizations

;; toggle advice: recenter the org buffer vertically after filling the paragraph
;; (M-q)
(defun org/center (&optional arg1 arg2)
  "Recenters the org buffer vertically and then tries to remove
horizontal shift to the right, that may appear with the input of
the long line.

It's purpose is to be used in advice to the `org-fill-paragraph',
so it takes the same number of parameters and ignores them."
  (progn
    (recenter)
    (backward-sentence)
    (forward-sentence)))

(defun org/autocenter ()
  (interactive)
  (if (advice-member-p 'org/center 'org-fill-paragraph)
      (progn
        (advice-remove 'org-fill-paragraph #'org/center)
        (message "Autocentering deactivated"))
    (progn
      (advice-add 'org-fill-paragraph :after #'org/center)
      (message "Autocentering activated"))))
