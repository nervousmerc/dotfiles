;;;; packages for programming languages and tooling


;;;; common packages

;; language server protocol (LSP) client
(use-package eglot
  :straight (:type built-in)
  :config
  (setq eglot-confirm-server-initiated-edits nil)
  (setq eldoc-idle-delay 0.75)
  (setq eglot-stay-out-of '(eldoc))
  (setq eldoc-echo-area-use-multiline-p nil)
  (setq-default eglot-workspace-configuration
                '((:gopls .
                          ((staticcheck . t)
                           (matcher . "CaseSensitive")))))
  :bind (("C-c e a" . eglot-code-actions)
         ("C-c e b" . eglot-format-buffer)
         ("C-c e f" . eglot-format)
         ("C-c e i" . imenu)
         ("C-c e r" . eglot-rename)
         ("C-c e R" . eglot-reconnect)
         ("C-c e q" . eglot-shutdown)
         ("C-c e Q" . eglot-shutdown-all)))


;;;; Clojure

;; support for the Clojure(Script) programming language (syntax highlighting,
;; etc)
(use-package clojure-mode
  :config
  (setq clojure-docstring-fill-column 70)
  :mode (("\\.cljr\\'" . clojure-mode)))

;; the next generation Clojure major mode for Emacs, powered by TreeSitter
;; (use-package clojure-ts-mode
;;   :straight
;;   (clojure-ts-mode :type git
;;                    :host github
;;                    :repo "clojure-emacs/clojure-ts-mode")
;;   :config
;;   (setq clojure-docstring-fill-column 70))


;; the Clojure(Script) Interactive Development Environment that Rocks!
(use-package cider
  :config
  ;; use symbol at point for reference lookup
  (setq cider-prompt-for-symbol nil)
  ;; automatically download all available .jars with Java sources and javadocs
  ;; for a given project (does not work with `deps.edn'-based projects yet)
  ;; (setq cider-enrich-classpath t)
  )


;; minor mode for interaction with Integrant REPL
(use-package integrant-repl-mode
  :straight
  (integrant-repl-mode :type git
                       :host gitlab
                       :repo "pigalev.k/el"
                       :local-repo "pk-el"
                       :depth 1
                       :files ("projects/integrant-repl-mode/src/*.el"))
  :init
  (setq integrant-repl-mode-command-map-prefix "C-c i")
  :hook
  (cider-mode . integrant-repl-mode))


;; minor mode for interaction with Kaocha test runner from REPL
(use-package kaocha-repl-mode
  :straight
  (kaocha-repl-mode :type git
                    :host gitlab
                    :repo "pigalev.k/el"
                    :local-repo "pk-el"
                    :depth 1
                    :files ("projects/kaocha-repl-mode/src/*.el"))
  :hook
  (cider-mode . kaocha-repl-mode))


;;;; Emacs Lisp

;; inspection tool for Emacs Lisp objects
(use-package inspector
  :config
  (bind-keys :map emacs-lisp-mode-map
             :prefix-map inspector-map
             :prefix "C-c i"
             ("i" . inspector-inspect-last-sexp)
             ("e" . inspector-inspect-expression)))


;; simple refactorings for Emacs Lisp
(use-package erefactor
  :defer t
  :config
  (define-key emacs-lisp-mode-map (kbd "C-c C-v") erefactor-map))


;;;; Common Lisp and Scheme

;; fork of SLIME
(use-package sly
  :config
  (setq sly-description-autofocus t)
  (setq sly-repl-shortcut-dispatch-char ?\?)
  (setq sly-lisp-implementations
        '((sbcl ("sbcl" "--noinform"))
          (abcl ("abcl" "--noinform"))
          (clisp ("clisp" "-q"))))
  (setq sly-default-lisp 'sbcl)
  :bind (:map sly-mode-map
              ("C-c b" . sly-eval-buffer)
              ("C-c M-M" . sly-macroexpand-1)
              ("C-c C-q" . sly-quit-lisp)))


;; a generic Scheme interaction mode
(use-package geiser)
(use-package geiser-guile)
(use-package geiser-racket)
(use-package geiser-chicken)

;; Simple Lisp Interpreter In Go
(use-package inf-lisp
  :straight (:type built-in)
  :custom
  (inferior-lisp-program "slingo")
  :bind
  (:map lisp-mode-map
        ("C-c <down>" . lisp-eval-last-sexp))
  :mode ("\\.sli\\'" . lisp-mode))

;;;; Others


;; javascript
(use-package js2-mode
  :diminish
  :config
  (setq js-indent-level 4)
  (add-hook 'js2-mode-hook #'js2-imenu-extras-mode)
  :mode ("\\.js\\'\\'" "\\.mjs\\'\\'")
  :bind
  (:map js2-mode-map
        ("C-c C-c" . compile)))


;; Lua
(use-package lua-mode
  :defer t)


;; Go
;; install language server: go install golang.org/x/tools/gopls@latest
(use-package go-mode
  :hook (before-save . gofmt-before-save)
  :bind
  (:map go-mode-map
        (("M-." . godef-jump)
         ("C-c i" . go-goto-imports)
         ("C-c C-r" . go-remove-unused-imports))))
