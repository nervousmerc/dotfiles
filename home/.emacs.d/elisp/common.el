;;;; global settings


(use-package emacs
  :init
  ;; enable indentation+completion using the TAB key; `completion-at-point' is
  ;; often bound to M-TAB
  (setq tab-always-indent 'complete)
  ;; avoid localized timestamps
  (setq system-time-locale "C")
  ;; switch splash, scratch text, toolbar, menubar and scrollbar off
  (setq inhibit-splash-screen t)
  (setq initial-scratch-message "")
  (tool-bar-mode -1)
  (menu-bar-mode -1)
  (scroll-bar-mode -1)
  ;; delete trailing whitespace before saving
  (add-hook 'before-save-hook 'delete-trailing-whitespace)
  ;; always insert spaces instead of tabs
  (setq-default indent-tabs-mode nil)
  ;; set window size and default font for all frames
  ;; (set-frame-font "Source Code Pro 13")
  ;; (set-frame-font "Iosevka Slab Extended 14")
  (add-to-list 'default-frame-alist '(width . 120))
  (add-to-list 'default-frame-alist '(font . "Iosevka Slab Extended 14"))
  ;; highlight search queries
  (setq search-highlight t)
  (setq query-replace-highlight t)
  ;; show matching parens
  (show-paren-mode 1)
  (setq show-paren-style 'mixed)
  ;; set default tab width and indent width
  (setq-default tab-width 4)
  (setq-default c-basic-offset 4)
  ;; set default fill column
  (setq-default fill-column 78)
  (setq-default whitespace-line-column 78)
  ;; show line and column numbers
  (setq line-number-display-limit-width 10000)
  (line-number-mode 1)
  (column-number-mode 1)
  ;; window configurations undo-redo (C-c left/right); winner is built-in in
  ;; emacs 27
  (if (config/old-emacs?)
      (use-package winner
        :diminish
        :config
        (winner-mode 1))
    (winner-mode 1))
  ;; make all backups in one place
  (setq backup-inhibited -1)
  (setq backup-directory-alist
        '(("." . (expand-file-name "backups" user-emacs-directory))))
  ;; tame the wild mouse
  (setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; by one line
  (setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
  (setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
  (setq scroll-step 1) ;; keyboard scroll one line at a time
  ;; fix mouse wheel behavior in Info mode
  (with-eval-after-load 'info
    (define-key Info-mode-map (kbd "<mouse-4>") 'mwheel-scroll)
    (define-key Info-mode-map (kbd "<mouse-5>") 'mwheel-scroll))
  ;; bind other-window to less cumbersome key
  (global-set-key (kbd "C-z") 'other-window)
  ;; an easy key to reread the current buffer from disk
  (global-set-key (kbd "<f5>") 'revert-buffer)
  ;; always short confirmation
  (defalias 'yes-or-no-p 'y-or-n-p)
  ;; fix point movement lag https://emacs.stackexchange.com/q/28736
  (setq auto-window-vscroll nil)
  ;; automatically revert buffers edited outside of emacs
  (global-auto-revert-mode 1)
  ;; a single space after the period is considered a sentence end
  (setq sentence-end-double-space nil)
  ;; allow to visit files from command line in an existing Emacs session
  (server-start))


;;;; common packages


;; modifier-free editing experience: use comma instead of Ctrl
(use-package devil
  :demand t
  :config
  (setq devil-lighter " \U0001F608")
  (setq devil-prompt "\U0001F608 %t")
  ;; customized to not repeat different movement commands in one go, as
  ;; default setting interferes with ordinary character insertion at times
  ;; (e.g., `, f e' moves one character forward and jumps to the end of the
  ;; line instead of moving one character forward and inserting `e')
  (setq devil-repeatable-keys
        '(("%k /") ("%k d") ("%k k") ("%k m ^")
          ("%k m e") ("%k m b") ("%k m f") ("%k m a") ("% k m e")
          ("%k m @" "%k m h")
          ("%k m y")
          ("%k p") ("%k n") ("%k b") ("%k f") ("%k a") ("%k e")
          ("%k s")
          ("%k x [" "%k x ]")
          ("%k x ^" "%k x {" "%k x }")
          ("%k x o")
          ("%k x u")))
  (global-devil-mode)
  :bind
  ("C-," . global-devil-mode))


;; desktop save and restore (frames, tabs, windows, buffers, positions etc)
(use-package desktop
  :straight (:type built-in)
  :demand t
  :config
  (desktop-save-mode 1))


;; named window configurations
(use-package tab-bar
  :straight (:type built-in)
  :demand t
  :init
  (tab-bar-mode 1))


;; quick file selection popup
(use-package speedbar
  :straight (:type built-in)
  :bind
  ("<f8>" . speedbar))


;; interactively show bound keys
(use-package which-key
  :diminish
  :config
  (setq which-key-popup-type 'side-window)
  (setq which-key-show-prefix 'left)
  (setq which-key-show-remaining-keys t)
  (define-key help-map "\C-h" 'which-key-C-h-dispatch)
  (which-key-mode))


;; remove some minor modes from modeline to save space
(use-package diminish
  :diminish auto-revert-mode
  :diminish eldoc-mode)


;; make keybindings work in the russian layout
(use-package reverse-im
  :config
  (reverse-im-activate "russian-computer"))


;; colorful parens
(use-package rainbow-delimiters
  :init (add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
        (setq rainbow-delimiters-max-face-count 9))


;; parens autoinsertion and structural editing
(use-package smartparens
  :diminish
  :bind (("<C-right>" . sp-forward-slurp-sexp)
         ("<C-left>" . sp-backward-slurp-sexp)
         ("<M-left>" . sp-forward-barf-sexp)
         ("<M-right>" . sp-backward-barf-sexp)
         ("<C-up>" . sp-backward-up-sexp)
         ("<C-down>" . sp-down-sexp)
         ("<M-up>" . sp-convolute-sexp)
         ("<M-down>" . sp-absorb-sexp)
         ("C-c C-w" . sp-kill-sexp)
         ("C-c M-w" . sp-copy-sexp))
  :init
  (require 'smartparens-config)
  :config
  (smartparens-global-mode 1))


;; undo tree
(use-package undo-tree
  :diminish
  :config
  (global-undo-tree-mode 1)
  (setq undo-tree-auto-save-history nil))


;; autocompletion
(use-package corfu
  :init
  (global-corfu-mode)
  :custom
  ;; enable autocompletion
  (corfu-auto t))


;; modeline
(use-package telephone-line
  :config
  (telephone-line-mode 1))


;; git frontend
(use-package magit
  :diminish
  :bind
  (("C-x g" . magit-status)))


;; git changes in the left fringe
(use-package git-gutter
  :diminish
  :config
  (global-git-gutter-mode +1))


;; completion & search (ivy + counsel and swiper)
;; collection of Ivy-enhanced versions of existing commands
(use-package counsel)
(use-package counsel-projectile)
;; generic completion mechanism
(use-package ivy
  :diminish
  :demand
  :bind (("M-x" . counsel-M-x)
         ("M-y" . counsel-yank-pop)
         ("C-s" . swiper-isearch)
         ("C-S-s" . swiper-thing-at-point)
         ("s-s" . counsel-projectile-ag)
         ("s-S" . counsel-git-grep)
         ("C-h a" . counsel-apropos)
         ("C-h b" . counsel-descbinds)
         ("C-h f" . counsel-describe-function)
         ("C-h v" . counsel-describe-variable)
         ("C-h l" . counsel-find-library)
         ("C-h i" . counsel-info-lookup-symbol)
         ("C-h u" . counsel-unicode-char)
         ("C-h j" . counsel-set-variable)
         ("C-c v" . ivy-push-view)
         ("C-c V" . ivy-pop-view)
         ("C-x r l" . counsel-bookmark)
         ("C-x r a" . counsel-register))
  :config
  (setq ivy-use-virtual-buffers t
        ivy-count-format "%d/%d "
        ivy-wrap t
        ivy-use-selectable-prompt t)
  (ivy-mode 1))
(use-package ivy-hydra)


;; icons
(use-package all-the-icons)


;; neotree panel, with icons
(use-package neotree
  :diminish
  :bind ([f9] . neotree-toggle)
  :init (setq neo-window-width 32)
  :config
  (setq neo-smart-open nil)
  (setq neo-theme (if (display-graphic-p)
                      'icons
                    'arrow)))


;; file manager
(use-package dired
  ;; it is a built-in package, do not try to fetch it
  :straight (:type built-in)
  :config
  ;; use only one buffer for dired
  (setq dired-kill-when-opening-new-dired-buffer t)
  ;; use dired in another window as destination for copying/renaming; previous
  ;; setting will prevent opening two different dired buffers though
  (setq dired-dwim-target t))

;; icons for dired
(use-package all-the-icons-dired
  :hook
  (dired-mode . all-the-icons-dired-mode))

;; show subdirectories in dired
(use-package dired-subtree
  :after dired
  :bind
  (:map dired-mode-map
        ([?\t] . dired-subtree-toggle)))


;; project management
(use-package projectile
  :diminish
  :config
  (projectile-mode 1)
  :bind-keymap
  (("C-c p" . projectile-command-map)
   ("s-p" . projectile-command-map)))

;; also project management (workaround for
;; https://github.com/radian-software/straight.el/issues/1146)
(use-package project
  :straight (:type built-in))

;; quick search (requires `ag' binary)
(use-package ag
  :config
  (setq ag-highlight-search t))


;; sync exec path with the system PATH
(use-package exec-path-from-shell
  :config
  (exec-path-from-shell-initialize))


;; semantic region selection and expansion
(use-package expand-region
  :bind
  (("C-=" . er/expand-region)))


;; eFAR 2-panel file manager
(use-package efar
  :defer t)


;; collection of small convenience functions
(use-package nuts-and-bolts
  :straight
  (nuts-and-bolts :type git
                       :host gitlab
                       :repo "pigalev.k/el"
                       :local-repo "pk-el"
                       :depth 1
                       :files ("components/nuts-and-bolts/*.el"))
  :bind ("C-<backspace>" . nuts-and-bolts/delete-backward-word))

;; language modes using tree-sitter
(use-package tree-sitter
  :init
  (add-to-list 'treesit-extra-load-path
             (expand-file-name "treesitter/" user-emacs-directory))
  :config
  (global-tree-sitter-mode 1))
