;;;; packages to handle various markup languages


;; markdown (pandoc should be installed)
(use-package markdown-mode
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "pandoc"))


;; LaTeX
(use-package auctex
  :defer t
  :config
  (setq-default TeX-engine 'xetex))


;; web-related markup
(use-package web-mode
  :diminish
  :defer t
  :config
  (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
  (setq web-mode-engines-alist
        '(("freemarker" . "\\.ftl\\'")))
  (setq web-mode-enable-current-element-highlight t)
  (setq web-mode-enable-current-column-highlight t))


;; JSON
(use-package json-mode
  :defer t
  :mode (("\\.json\\'" . json-mode)
         ("\\.tmpl\\'" . json-mode)
         ("\\.eslintrc\\'" . json-mode))
  :config
  (setq-default js-indent-level 2))

(use-package emmet-mode
  :defer t
  :hook
  ((html-mode sgml-mode css-mode web-mode) . 'emmet-mode))


;; docker control files
(use-package dockerfile-mode
  :defer t)


;; generic YAML
(use-package yaml-mode
  :defer t)


;;  CLI to transform between JSON, EDN, YAML and Transit using Clojure
(use-package jet
  :defer t
  :config
  (global-set-key (kbd "C-x j") 'jet))

;; EPUB reader
(use-package nov
  :config
  (setq nov-variable-pitch nil)
  :mode
  (("\\.epub\\'" . nov-mode)))
