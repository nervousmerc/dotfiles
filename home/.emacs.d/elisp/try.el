;;;; packages to try and evaluate


;;;; CV exporting packages (forks)

;; org exporter for curriculum vitae
(use-package ox-moderncv
  :straight
  (ox-moderncv :type git
               :host gitlab
               :repo "pigalev.k/org-cv"
               :local-repo "org-cv"))

(use-package ox-awesomecv
  :straight
  (ox-awesomecv :type git
                :host gitlab
                :repo "pigalev.k/org-cv"
                :local-repo "org-cv"))

;; org exporter for curriculum vitae (LaTeX)
(use-package ox-latex-cv
  :straight
  (ox-latex-cv :type git
               :host gitlab
               :repo "pigalev.k/el"
               :local-repo "pk-el"
               :depth 1
               :files ("components/ox-latex-cv/src/*.el")))


;;;; mail packages

;; CLI for indexing, searching, reading, and tagging large collections of
;; email messages
(use-package notmuch)

;; scheduling days with timeblocking technique
(use-package org-timeblock
  :straight
  (org-timeblock :type git
                 :host github
                 :repo "ichernyshovvv/org-timeblock")
  :config
  (setq org-timeblock-inbox-file
        (expand-file-name
         "~/Documents/workspace/el/projects/hello-elisp/src/org-timeblock.org")))


;; unofficial client for Telegram platform for GNU Emacs
(use-package telega
  :defer t
  :init
  (setq telega-use-docker t)
  (setq telega-chat-fill-column 70)
  :bind-keymap
  ("C-c t" . telega-prefix-map))

;; edit multiple occurrences of some text simultaneously
;; C-; to start or finish edit the text at point
(use-package iedit)

;; dialect of Lisp embedded in Python
(use-package hy-mode)

;; dialect of Lisp that compiles to Lua
(use-package fennel-mode)

(use-package git-timemachine
  :bind
  ("C-x G" . git-timemachine))

(use-package org-download
  :after org
  :config
  (setq-default org-download-heading-lvl nil)
  (setq-default org-download-image-dir "./images")
  :bind
  (:map org-mode-map
        (("s-Y" . org-download-screenshot)
         ("s-y" . org-download-yank))))

;; a minor mode to automatically balance window margins
(use-package olivetti)
