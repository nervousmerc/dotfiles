# emacs as a default editor
# $EDITOR should open in terminal
export EDITOR="emacsclient -t"
# $VISUAL opens in GUI with non-daemon as alternate
export VISUAL="emacsclient -c -a emacs"

# restic
export RESTIC_REPOSITORY=~/Documents/backups
export RESTIC_PASSWORD_COMMAND="pass backups/restic"
